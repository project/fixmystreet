FixMyStreet
===========

A Drupal-based re-creation of http://fixmystreet.com (UK) arising from the Open Government New Zealand initiative (refer http://open.org.nz/).

Visitors can report an Issue (e.g. pothole) to a Jurisdiction (i.e. a Council). Once confirmed by the submitter, notification of an Issue is emailed to the relevant jurisdicton for resolution. Issues can include a latitude and longitude, category, description and a photo.

Visitors can add Updates (comments) to an Issue, and indicate that the Issue is fixed.

To minimise usage hurdles, user accounts are not required. To minimise spam, confirmation by email is required for new Issues and Updates.

The code has been developed to support http://fixmystreet.org.nz in New Zealand, but I've tried to keep the implementation generic to enable use in other countries. The work so far has focussed on a reasonably faithful implementation of the original UK FixMyStreet.com. Future versions are like to diverge, adding new features and different interfaces.

The site uses multiple web services:
Google Maps: for map display, with markers representing Issues.
http://maps.google.com/maps/geo: address geocoding to latitude and longitude.
http://geonames.org: postcode geocoding to latitude and longitude.
http://koordinates.com: reverse geocode API to find Jurisdiction from from latitude and longitude.

Installation
============

Requires:
- photos/ directory in the site files/ directory.
- Google Maps API key (for Google Map display and geocoding).
- Koordinates.com API key (used for reverse-geocoding in NZ).
- Uses geonames.org for NZ postcode lookup.

Install FixMyStreet, FixMyStreet Geo, FixMyStreet Import, FixMyStreet Issue, FixMyStreet Jurisdiction, FixMyStreet Reports.

Enable FixMyStreet Confirm by Email action module.
  In Site configuration > Actions > Manage actions, enable and configure actions:
    Issue a confirmation token by email to the node author, configure to Issue content type.
    Issue a confirmation token by email to the comment author, configure to Issue content type.
  In Site building > Triggers > Content, when saving a new post, send confirmation email to node author.
  In Site building > Triggers > Comments, when saving a new post, send confirmation email to comment author.

Administration
Ensure cron is configured to remove unconfirmed Issues and Updates.

Promote an Issue to homepage to have it appear in the list of photos.

Architecture
============

fixmystreet_about.module
- Hard-coded text about FixMyStreet NZ (on paths /about, /privacy). Used to ease migration from dev to live.

fixmystreet_confirmbyemail.module
- Email a token to submitter of an Issue or Update. When the token URL is invoked, move the Issue to Confirmed, or publish the comment.

fixmystreet_geo.module
- Postcode geocode (via Geonames)
- Geocode (via Google)
- Reverse geocode (via Koordinates.com)

fixmystreet_import.module
- Implements simple HTTP POST API used by iPhone, Android apps.
- Deprecated in favour of Open311 API.

fixmystreet_issue.module
- Feature defining Issue content type.

fixmystreet_jurisdiction.module
- Feature defining Jurisdiction content type.

fixmystreet.module
- Set country code and country name.
- Set territorial authority name (where Issues are sent).
- Postcode validation
- latlon bounds checking.

fixmystreet_reports.module
- Feature defining reports including views for lists of Issues, Updates.
- homepage boxes for recently added Issues, recently fixed, updates to Issues. 

imagecache_autorotate.module
- Define imagecache action to auto-rotate images received from mobiles via FixMyStreet import API.
- Rotation is based on embedded EXIF data.
- Might be replaced by a newer version of Imagecache Actions.

Config variables:
fixmystreet_country_code: e.g. nz
fixmystreet_country_name: e.g. New Zealand
fixmystreet_jurisdiction_type: e.g. Council
fixmystreet_postcode_length: e.g. 4

Modifying for reuse in another country
======================================
@todo

Hooks
=====
hook_fixmystreet_map_jurisdiction() - allow modules to modify the choice of jurisdiction, e.g. to handle merged jurisdictions.

Theme
=====
The theme in use at http://fixmystreet.org.nz is by Josh Campbell. It uses icons from http://famfamfam.com/lab/icons/silk/. It should only be used for New Zealand.

Roadmap
=======
@todo
- feature-ize stuff.
- create hooks to ease reuse in new countries.
- Complete transition from Territorial Authority (TA) to Jurisdiction (in line with Open311 terminology).
- auto-create photos dir.
- allow jurisdiction staff to add tracking codes.
- RSS/ATOM?
- email notifications
- flag offensive content
- Can Imagecache Actions replace imagecache_autorotate.module?
API
- Add support for Open311 GeoReport v2 API

<?php
/**
 * @file
 * Form builder for FixMyStreet settings.
 *
 * @ingroup forms
 * @see system_settings_form()
 */

function fixmystreet_admin_settings() {
  $form['fixmystreet_country_code'] = array(
    '#type' => 'textfield',
    '#title' => t('Country code'),
    '#description' => t('Two letter, lowercase code from <a href="@codes_url">@codes_url</a>', array(
      '@codes_url' => url('http://code.google.com/apis/maps/documentation/geocoding/', array('fragment' => 'CountryCodes')),
    )),
    '#default_value' => variable_get('fixmystreet_country_code', 'nz'),
    '#size' => 2,
  );

  $form['fixmystreet_country_name'] = array(
    '#type' => 'textfield',
    '#title' => t('Country name'),
    '#description' => t('Country name corresponding to code above.'),
    '#default_value' => variable_get('fixmystreet_country_name', 'New Zealand'),
  );

  $form['fixmystreet_jurisdiction_type'] = array(
    '#type' => 'textfield',
    '#title' => t('Type of organisation receiving issues'),
    '#description' => t('Open311 Jurisdiction type e.g. Council, Territorial Authority. Used in help text, url paths, etc.'),
    '#default_value' => variable_get('fixmystreet_jurisdiction_type', 'Council'),
  );

  $form['fixmystreet_test'] = array(
    '#type' => 'checkbox',
    '#title' => t('Enable test mode'),
    '#description' => t('Emails to territorial authorities are diverted to the original report poster.'),
    '#default_value' => variable_get('fixmystreet_test', TRUE),
  );

  $form['fixmystreet_test_message'] = array(
    '#type' => 'textarea',
    '#title' => t('Message to display for test mode'),
    '#description' => t('Available tokens: @jurisdiction_type'),
    '#default_value' => variable_get('fixmystreet_test_message', ''),
  );

  return system_settings_form($form);
}

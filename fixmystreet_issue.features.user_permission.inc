<?php

/**
 * Implementation of hook_user_default_permissions().
 */
function fixmystreet_issue_user_default_permissions() {
  $permissions = array();

  // Exported permission: create issue content
  $permissions['create issue content'] = array(
    'name' => 'create issue content',
    'roles' => array(
      '0' => 'Admin',
      '1' => 'Designer',
      '2' => 'Site Admin',
      '3' => 'anonymous user',
    ),
  );

  // Exported permission: delete own issue content
  $permissions['delete own issue content'] = array(
    'name' => 'delete own issue content',
    'roles' => array(
      '0' => 'Site Admin',
    ),
  );

  // Exported permission: edit any issue content
  $permissions['edit any issue content'] = array(
    'name' => 'edit any issue content',
    'roles' => array(
      '0' => 'Site Admin',
    ),
  );

  // Exported permission: edit field_category
  $permissions['edit field_category'] = array(
    'name' => 'edit field_category',
    'roles' => array(
      '0' => 'Site Admin',
      '1' => 'anonymous user',
    ),
  );

  // Exported permission: edit field_email
  $permissions['edit field_email'] = array(
    'name' => 'edit field_email',
    'roles' => array(
      '0' => 'Site Admin',
      '1' => 'anonymous user',
    ),
  );

  // Exported permission: edit field_firstname
  $permissions['edit field_firstname'] = array(
    'name' => 'edit field_firstname',
    'roles' => array(
      '0' => 'Site Admin',
      '1' => 'anonymous user',
      '2' => 'authenticated user',
    ),
  );

  // Exported permission: edit field_lastname
  $permissions['edit field_lastname'] = array(
    'name' => 'edit field_lastname',
    'roles' => array(
      '0' => 'Site Admin',
      '1' => 'anonymous user',
      '2' => 'authenticated user',
    ),
  );

  // Exported permission: edit field_location
  $permissions['edit field_location'] = array(
    'name' => 'edit field_location',
    'roles' => array(
      '0' => 'Site Admin',
      '1' => 'anonymous user',
    ),
  );

  // Exported permission: edit field_location_initial
  $permissions['edit field_location_initial'] = array(
    'name' => 'edit field_location_initial',
    'roles' => array(
      '0' => 'Site Admin',
    ),
  );

  // Exported permission: edit field_phone
  $permissions['edit field_phone'] = array(
    'name' => 'edit field_phone',
    'roles' => array(
      '0' => 'Site Admin',
      '1' => 'anonymous user',
    ),
  );

  // Exported permission: edit field_photo
  $permissions['edit field_photo'] = array(
    'name' => 'edit field_photo',
    'roles' => array(
      '0' => 'Site Admin',
      '1' => 'anonymous user',
      '2' => 'authenticated user',
    ),
  );

  // Exported permission: edit field_public
  $permissions['edit field_public'] = array(
    'name' => 'edit field_public',
    'roles' => array(
      '0' => 'Site Admin',
      '1' => 'anonymous user',
    ),
  );

  // Exported permission: edit field_ta
  $permissions['edit field_ta'] = array(
    'name' => 'edit field_ta',
    'roles' => array(
      '0' => 'Site Admin',
    ),
  );

  // Exported permission: edit field_ta_name
  $permissions['edit field_ta_name'] = array(
    'name' => 'edit field_ta_name',
    'roles' => array(
      '0' => 'Site Admin',
    ),
  );

  // Exported permission: edit field_url
  $permissions['edit field_url'] = array(
    'name' => 'edit field_url',
    'roles' => array(
      '0' => 'Site Admin',
    ),
  );

  // Exported permission: edit own issue content
  $permissions['edit own issue content'] = array(
    'name' => 'edit own issue content',
    'roles' => array(
      '0' => 'Site Admin',
    ),
  );

  // Exported permission: upload files
  $permissions['upload files'] = array(
    'name' => 'upload files',
    'roles' => array(
      '0' => 'Admin',
      '1' => 'Designer',
      '2' => 'Site Admin',
      '3' => 'anonymous user',
      '4' => 'authenticated user',
    ),
  );

  // Exported permission: view field_category
  $permissions['view field_category'] = array(
    'name' => 'view field_category',
    'roles' => array(
      '0' => 'Site Admin',
      '1' => 'authenticated user',
    ),
  );

  // Exported permission: view field_email
  $permissions['view field_email'] = array(
    'name' => 'view field_email',
    'roles' => array(
      '0' => 'Site Admin',
    ),
  );

  // Exported permission: view field_email_roads
  $permissions['view field_email_roads'] = array(
    'name' => 'view field_email_roads',
    'roles' => array(
      '0' => 'Site Admin',
    ),
  );

  // Exported permission: view field_firstname
  $permissions['view field_firstname'] = array(
    'name' => 'view field_firstname',
    'roles' => array(
      '0' => 'Site Admin',
    ),
  );

  // Exported permission: view field_lastname
  $permissions['view field_lastname'] = array(
    'name' => 'view field_lastname',
    'roles' => array(
      '0' => 'Site Admin',
    ),
  );

  // Exported permission: view field_location
  $permissions['view field_location'] = array(
    'name' => 'view field_location',
    'roles' => array(
      '0' => 'Site Admin',
      '1' => 'anonymous user',
    ),
  );

  // Exported permission: view field_location_initial
  $permissions['view field_location_initial'] = array(
    'name' => 'view field_location_initial',
    'roles' => array(
      '0' => 'Site Admin',
    ),
  );

  // Exported permission: view field_phone
  $permissions['view field_phone'] = array(
    'name' => 'view field_phone',
    'roles' => array(
      '0' => 'Site Admin',
    ),
  );

  // Exported permission: view field_photo
  $permissions['view field_photo'] = array(
    'name' => 'view field_photo',
    'roles' => array(
      '0' => 'Site Admin',
      '1' => 'anonymous user',
    ),
  );

  // Exported permission: view field_public
  $permissions['view field_public'] = array(
    'name' => 'view field_public',
    'roles' => array(
      '0' => 'Site Admin',
    ),
  );

  // Exported permission: view field_ta
  $permissions['view field_ta'] = array(
    'name' => 'view field_ta',
    'roles' => array(
      '0' => 'Site Admin',
      '1' => 'anonymous user',
      '2' => 'authenticated user',
    ),
  );

  // Exported permission: view field_ta_name
  $permissions['view field_ta_name'] = array(
    'name' => 'view field_ta_name',
    'roles' => array(
      '0' => 'Site Admin',
    ),
  );

  // Exported permission: view imagecache original
  $permissions['view imagecache original'] = array(
    'name' => 'view imagecache original',
    'roles' => array(
      '0' => 'Site Admin',
    ),
  );

  // Exported permission: view imagecache preview
  $permissions['view imagecache preview'] = array(
    'name' => 'view imagecache preview',
    'roles' => array(
      '0' => 'Admin',
      '1' => 'Designer',
      '2' => 'Site Admin',
      '3' => 'anonymous user',
      '4' => 'authenticated user',
    ),
  );

  // Exported permission: view imagecache thumbnail
  $permissions['view imagecache thumbnail'] = array(
    'name' => 'view imagecache thumbnail',
    'roles' => array(
      '0' => 'Admin',
      '1' => 'Designer',
      '2' => 'Site Admin',
      '3' => 'anonymous user',
      '4' => 'authenticated user',
    ),
  );

  return $permissions;
}

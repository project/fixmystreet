/* Add toggle for display of issue details */

Drupal.behaviors.details = function(context) {
  $('.details').hide();
  $('.more-details').append('<a id="details-toggle" href="#">see more details</a> or ').click(function() {
    $('.details').toggle()
  });
};

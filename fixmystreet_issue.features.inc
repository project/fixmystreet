<?php

/**
 * Implementation of hook_ctools_plugin_api().
 */
function fixmystreet_issue_ctools_plugin_api() {
  list($module, $api) = func_get_args();
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => 1);
  }
}

/**
 * Implementation of hook_imagecache_default_presets().
 */
function fixmystreet_issue_imagecache_default_presets() {
  $items = array(
    'original' => array(
      'presetname' => 'original',
      'actions' => array(
        '0' => array(
          'weight' => '0',
          'module' => 'imagecache_autorotate',
          'action' => 'imagecache_autorotate',
          'data' => array(),
        ),
      ),
    ),
    'preview' => array(
      'presetname' => 'preview',
      'actions' => array(
        '0' => array(
          'weight' => '-10',
          'module' => 'imagecache_autorotate',
          'action' => 'imagecache_autorotate',
          'data' => FALSE,
        ),
        '1' => array(
          'weight' => '-9',
          'module' => 'imagecache',
          'action' => 'imagecache_scale',
          'data' => array(
            'width' => '300',
            'height' => '300',
            'upscale' => 0,
          ),
        ),
      ),
    ),
    'thumbnail' => array(
      'presetname' => 'thumbnail',
      'actions' => array(
        '0' => array(
          'weight' => '-10',
          'module' => 'imagecache_autorotate',
          'action' => 'imagecache_autorotate',
          'data' => array(),
        ),
        '1' => array(
          'weight' => '-9',
          'module' => 'imagecache',
          'action' => 'imagecache_scale_and_crop',
          'data' => array(
            'width' => '100',
            'height' => '100',
          ),
        ),
      ),
    ),
  );
  return $items;
}

/**
 * Implementation of hook_node_info().
 */
function fixmystreet_issue_node_info() {
  $items = array(
    'issue' => array(
      'name' => t('Issue'),
      'module' => 'features',
      'description' => t('An issue that needs to be fixed, with a location.'),
      'has_title' => '1',
      'title_label' => t('Subject'),
      'has_body' => '1',
      'body_label' => t('Detail'),
      'min_word_count' => '0',
      'help' => t('You have located the issue at the point marked with a red pin on the map. If this is not the correct location, simply click on the map again.

Please fill in details of the issue below. The council won\'t be able to help unless you leave as much detail as you can, so please describe the exact location of the problem (e.g. on a wall), what it is, how long it has been there, a description (and a photo of the problem if you have one), etc. '),
    ),
  );
  return $items;
}

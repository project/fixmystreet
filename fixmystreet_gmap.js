/* Use gmap handler to fetch nearby Issues via an AJAX call when map is moved */
/* http://drupal.org/node/150939#comment-1285426 */

Drupal.behaviors.gmap_init = function(context) {
  Drupal.gmap.addHandler('gmap', function(elem) {
    var obj = this;
    // @todo: try 'idle' here.
    obj.bind('move', function() {
      var params = '?lat=' + obj.vars.latitude + '&lon=' + obj.vars.longitude + '&' + 1*new Date(); // break browser cache
      $('#nearby-issues-list').load('/nearby' + params +' #nearby-issues-list')
    });
  });

  Drupal.gmap.addHandler('gmap', function(elem) {
    var obj = this;

    obj.bind('ready', function() {
      // Init locpick and issue gmap.
      if (obj.map) {
        map = obj.map; // Put map in global scope for createMarker().
        initDirection(obj.map);
      }
    });
  });

};

// Init gmap.
function initDirection(map) {
  //map = Drupal.gmap.getMap('gmap-loc1-gmap0').map;

  var center = map.getCenter();
  // Add markers to initial map.
  getMarkers(center);

  // When map is moved, re-fetch markers.
  GEvent.addListener(map, "moveend", function() {
    var center = map.getCenter();

    /*var bounds = map.getBounds();
    var southWest = bounds.getSouthWest();
    var northEast = bounds.getNorthEast();
    var lngSpan = northEast.lng() - southWest.lng();
    var latSpan = northEast.lat() - southWest.lat();
    for (var i = 0; i < 2; i++) {
      var point = new GLatLng(southWest.lat() + latSpan * Math.random(),
      southWest.lng() + lngSpan * Math.random());
      map.addOverlay(createMarker(point, i + 1));
    }*/

    getMarkers(center);
  });

}

function getMarkers(center) {
  $.ajax({type: 'GET',
    url: "/nearby-markers/",
    dataType: 'json',
    success: makeMarkers,
    data: {lat: center.lat(), lon: center.lng(), hash: 1*new Date()} // break browser cache'
  });
}

function makeMarkers(data){
  //alert('data' + data);
  var map = Drupal.gmap.getMap('gmap-loc1-gmap0').map;

  if (!map) {
    var map = Drupal.gmap.getMap('gmap-auto1map-gmap0').map;
  }

  // Need to make this smarter, and only load markers we don't already have
  //map.clearOverlays();

  var list="<p>";
  for(var i = 0; i < data.length; i++){
    var point = new GLatLng(data[i]['point']['lat'], data[i]['point']['lon']);
    var marker = createMarker(point, data[i]['title'], data[i]['status'], data[i])
    map.addOverlay(marker);
    list += "<div class='entry'>"+ data[i]['title'] +'</div>';
  }
  list += '</p>';
  // debug $("#footer").html(list);
}

function createMarker(point, title, status, d) {
  // Create our "tiny" marker icon
  var tinyIcon = new GIcon(G_DEFAULT_ICON);
  if (status == 'Fixed') {
    tinyIcon.image = "/" + Drupal.settings.fixmystreet.fixmystreet_gmap_path + "/markers/green.png";
  }
  else {
    tinyIcon.image = "/" + Drupal.settings.fixmystreet.fixmystreet_gmap_path + "/markers/orange.png";
  }
  //tinyIcon.iconSize = new GSize(12, 20);
  //tinyIcon.shadow = "http://labs.google.com/ridefinder/images/mm_20_shadow.png";
  tinyIcon.shadow = '/' + Drupal.settings.fixmystreet.fixmystreet_gmap_path + '/markers/big/shadow.png';
  //tinyIcon.shadowSize = new GSize(22, 20);
//  tinyIcon.iconAnchor = new GPoint(6, 20);
//  tinyIcon.infoWindowAnchor = new GPoint(5, 1);
  markerOptions = { icon:tinyIcon };

  //alert('point' + point + ', number=' + number);
  var marker = new GMarker(point, markerOptions);
  var message = ["This","is","the","secret","message"];
  marker.value = title;
  marker.title = title;

  GEvent.addListener(marker, 'click', function() {
    var myHtml = '<span class="issue-title"><a href="/issue/' + d['nid'] + '">' + title + '</a></span><br/>#' + d['nid'];
    map.openInfoWindowHtml(point, myHtml);
  });
  //GEvent.addListener(marker, "mouseout", function() {
  //  map.closeInfoWindow();
  //});
  return marker;
}

<?php
/**
 * @file
 * Geocode from address to lat/lon via Google.
 * Geocode from postcode to lat/lon via postcode db files.
 * Reverse-geocode from lat/lon to jurisdiction via Koordinates API.
 * Do Gmap stuff.
 *
 * @author Jonathan Hunt fixmystreet_geo.module@huntdesign.co.nz
 *
 */
define('G_GEO_SUCCESS', 200);
define('G_GEO_UNKNOWN_ADDRESS', 602);

/**
 * Find nearby issues (within approx 2km) in state of Confirmed or Fixed.
 * See http://en.wikipedia.org/wiki/Geographic_coordinate_system
 * @todo: If we had a distance function, we could order them by closest first.
 */
function fixmystreet_geo_nearby_issues($lat, $lon) {
  $output = '';

  $confirmed_sid = fixmystreet_sid('Confirmed');
  $fixed_sid = fixmystreet_sid('Fixed');

  db_query("SET @center = GeomFromText('POINT(%s %s)');", (float)$lon, (float)$lat);
  // Very rough calculation via GoogleEarth shows that in Christchurch +/-1km = 0.009 lat and 0.013 lon
  // Compromise on 0.02 approx. 2km. FixMyStreet uses 2.7km
  db_query("SET @radius = 0.02;"); // @todo How does the radius relate to km?
  db_query("SET @bbox = GeomFromText(CONCAT('POLYGON((',
    X(@center) - @radius, ' ', Y(@center) - @radius, ',',
    X(@center) + @radius, ' ', Y(@center) - @radius, ',',
    X(@center) + @radius, ' ', Y(@center) + @radius, ',',
    X(@center) - @radius, ' ', Y(@center) + @radius, ',',
    X(@center) - @radius, ' ', Y(@center) - @radius, '))'));"
  );
  $result = db_query("SELECT r.nid, astext(field_location_geo) FROM {content_type_issue} r LEFT JOIN {workflow_node} w ON r.nid=w.nid WHERE (sid=%d OR sid=%d) AND MBRContains(@bbox, field_location_geo) AND astext(field_location_geo) <> 'POINT(%s %s)' LIMIT %d;", $confirmed_sid, $fixed_sid, (float)$lon, (float)$lat, variable_get('fixmystreet_max_nearby_issues', 10));
  $nodes = array();
  while ($node = db_fetch_object($result)) {
    $node = node_load($node->nid);
    $nodes[$node->nid] = $node;
  }
  return $nodes;
}

/**
 * Returns TRUE if coordinates are within the country bounds.
 */
function fixmystreet_within_bounds($lat, $lon, $country) {
  $ok = TRUE;
  switch ($country) {
    // Simplistic bounds checking for now. This includes Stewart Island, but not Chathams.
    case 'nz':
      $lat_min = -47;
      $lat_max = -34;
      $lon_min = 166;
      $lon_max = 179;
      if ($lat < $lat_min) { // Minimum latitude.
        $ok = FALSE;
      }
      if ($lat > $lat_max) { // Maximum latitude.
        $ok = FALSE;
      }
      if ($lon < $lon_min) { // Minimum longitude.
        $ok = FALSE;
      }
      if ($lon > $lon_max) { // Maximum longitude.
        $ok = FALSE;
      }
      break;
  }

  return $ok;
}

/**
 * @param $input postal code for geocoding
 * @param $country is per http://code.google.com/apis/maps/documentation/geocoding/#CountryCodes
 * Use geonames.org to return lat lon for postcode.
 * http://www.geonames.org/export/web-services.html#postalCodeLookupJSON
 */
function fixmystreet_geo_geocode_postcode($postalcode, $country, $options = array()) {

  switch ($country) {
    case 'nz':
      $url = url('http://ws.geonames.org/postalCodeLookupJSON', array(
        'query' => array(
          'postalcode' => $postalcode,
          'country' => $country,
        ),
      ));
      $headers = array();
    
      $response = drupal_http_request($url, $headers);
      if ($response->code == 200) {
        $result = json_decode($response->data);
        if (empty($result->postalcodes)) {
          drupal_set_message(t('Could not find that postcode.'), 'error');
        }
        return $result;
      }
      break;
  }

  drupal_set_message(t('Sorry, there was a problem with the service to interpret postcodes. Try entering a street address, or come back later.'), 'error');
  watchdog('fixmystreet', 'Postcode geocode issue: %code from @data', array(
    '%code' => $response->code,
    '@data' => $postalcode,
  ));
}

/**
 * @param $input is street address for geocoding
 * @param $country is per http://code.google.com/apis/maps/documentation/geocoding/#CountryCodes
 * Based on geocode.module.
 */
function fixmystreet_geo_geocode($input, $country, $options = array()) {
  //flog_it(__FUNCTION__ .': input='. print_r($input, TRUE) .', options='. print_r($options, TRUE));

  if (is_array($input)) {
    $input = join(',', $input);
  }

  $url = url('http://maps.google.com/maps/geo', array(
    'query' => array(
      'q' => urlencode($input),
      'gl' => $country,
      'sensor' => 'false',
      'oe' => 'utf8',
      'output' => 'json',
      'key' => variable_get('googlemap_api_key', ''), // Google API key.
    ),
    'absolute' => TRUE,
    'external' => TRUE,
  ));

  $headers = array();

  //flog_it(__FUNCTION__ .': url='. $url);
  $response = drupal_http_request($url, $headers);
  //flog_it(__FUNCTION__ .': response='. print_r($response, TRUE));
  if ($response->code == 200) {
    $result = json_decode($response->data);

    // Handle response per http://code.google.com/apis/maps/documentation/geocoding/#GeocodingResponses
    // and http://code.google.com/apis/maps/documentation/geocoding/#StatusCodes
    if ($result->Status->code == G_GEO_SUCCESS) {
      // Iterate through and remove an placemarks outside our preferred country.
      $country_code = variable_get('fixmystreet_country_code', 'nz');
      if (count($result->Placemark)) {
        foreach ($result->Placemark as $key => $placemark) {
          //flog_it(__FUNCTION__ .': placemark code='. $placemark->AddressDetails->Country->CountryNameCode);
          if (strtolower($placemark->AddressDetails->Country->CountryNameCode) != $country_code) {
            unset($result->Placemark[$key]);
          }
        }
      }
      // @todo: check for number of results. If we still have some results, return them.
      return $result;
    }
    else {
      switch ($result->Status->code) {
        case G_GEO_UNKNOWN_ADDRESS:
          drupal_set_message(t('No corresponding geographic location could be found for the specified address.'), 'error');
          break;
      }
    }
  }
  else {
    drupal_set_message(t('There was a problem resolving the location.'), 'error');
  }

  return FALSE;
}

/**
 * Return the Jurisdiction name based on lat, lon via the Koordinates API.
 http://api.koordinates.com/api/0.1/reverseGeocode.xml/?key=<key>&layer=198&x=174.79419708251953&y=-41.24090006286729
 * Layer 198 is http://koordinates.com/layer/198-nz-territorial-authorities-2008/
 * Docs http://docs.google.com/View?id=ddx7tw85_21g28783fp
 */
function fixmystreet_geo_reverse_geocode_jurisdiction($lat, $lon, $country) {

  if (empty($lat)) {
    return drupal_set_message('error', t('No latitude supplied, so I do not know which @jurisdiction_type to refer this issue to.', array(
      '@jurisdiction_type' => variable_get('fixmystreet_jurisdiction_type', 'Council'),
    )));
  }

  switch ($country) {
    case 'nz':
      $url = url('http://api.koordinates.com/api/0.1/reverseGeocode.xml/', array(
        'query' => array(
          'y' => $lat,
          'x' => $lon,
          'layer' => '198', // NZ territorial authorities
          'key' => variable_get('koordinates_api_key', ''), // Koordinates API key via settings.php.
        ),
        'absolute' => TRUE,
        'external' => TRUE,
      ));

      $response = drupal_http_request($url);
      if ($response->code == 200) {
        $xml = new SimpleXMLElement($response->data);
        $name = $xml->xpath('//NAME');
        $name = (string)$name[0];
        return $name;
      }
      break;
  }

  return FALSE; // Could not match a Jurisdiction.
}

/**
 * Return array of lat and lon for node.
 * @todo: there may be a way in geo to do this...
 */
function fixmystreet_geo_latlon($node) {
  module_load_include('inc', 'geo', 'includes/geo.wkb');
  $wkb = $node->field_location[0]['wkb'];
  $wkb_data = _geo_wkb_get_data($wkb, 'wkt');
  return array('lat' => $wkb_data['y'], 'lon' => $wkb_data['x']);
}
